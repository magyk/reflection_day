<!--

Write a table that creates a db if one isn't present locally.

PDO - Connect to DB


https://www.w3schools.com/php/default.asp
-->




<!-- HEADER -->
<?php include "page-partials/header.php"; ?>

<!-- TOP NAV -->
<?php include "page-partials/top-nav.php"; ?>

<!-- MAIN NAV -->
<?php include "page-partials/main-nav.php"; ?>

<!-- PURPLE BANNER  -->
<?php include 'page-partials/purple-banner.php'; ?>

<!-- POPULAR FRANCHISES -->
<?php include "page-partials/popular-franchise.php"; ?>

<!-- ORANGE BANNER -->
<?php include 'page-partials/orange-banner.php'; ?>

<!-- FEATURED FRANCHISES -->
<?php include 'page-partials/featured-franchises.php'; ?>

<!-- BLUE BANNER -->
<?php include 'page-partials/blue-banner.php'; ?>

<!-- FOOTER -->
<?php include 'page-partials/footer.php'; ?>

