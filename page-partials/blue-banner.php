<div class="blue-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="card-deck">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-deck-header">
                                <h1>Latest News Articles</h1>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="blue-card-img">
                                    <img src="img/latest_news_articles/1.png">
                                </div>
                                <p>Its that time of year again. National Pet Month runs from 1st April to 1st May 2017...</p>
                                <button type="button" class="btn">Read More ></button>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="blue-card-img">
                                    <img src="img/latest_news_articles/2.png">
                                </div>
                                <p>Leading pizza franchise, Papa John’s, has announced its youngest franchisee this week...</p>
                                <button type="button" class="btn">Read More ></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card-deck">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-deck-header">
                                <h1>Latest Case Studies</h1>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="blue-card-img">
                                    <img src="img/latest_case_studies/1.png">
                                </div>
                                <p>As Suit the City enters its tenth year, Franchisee Tony Carr has reported average spend by clients is...</p>
                                <button type="button" class="btn">Read More ></button>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="blue-card-img">
                                    <img src="img/latest_case_studies/2.png">
                                </div>
                                <p>Franchisee review of Equitimax Insiders CircleIan joined EIC in October 2016. Previously a Company Director...</p>
                                <button type="button" class="btn">Read More ></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row blue-row-2">

            <div class="col-md-6">
                <div class="card-deck">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-deck-header">
                                <h1>Discovery Days</h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="blue-card-img">
                                    <img src="img/discovery_days/1.png">
                                </div>
                                <p>If you are investigating care sector franchises, we would love you to come and meet the team at...</p>
                                <button type="button" class="btn">Read More ></button>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="blue-card-img">
                                    <img src="img/discovery_days/2.png">
                                </div>
                                <p>London 1-2-1 meetingsInterested in finding out more about joining the TaxAssist Accountants network...</p>
                                <button type="button" class="btn">Read More ></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card-deck">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-deck-header">
                                <h1>Media Partners</h1>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="blue-card-img">
                                    <img src="img/media_partners/1.png">
                                </div>
                                <p>Some franchisors just can't help but shake things up and this month’s Elite Franchise cover star...</p>
                                <button type="button" class="btn">Read More ></button>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="blue-card-img">
                                    <img src="img/media_partners/2.png">
                                </div>
                                <p>Looking to become your own boss? You may have an extraordinary entrepreneurial spirit...</p>
                                <button type="button" class="btn">Read More ></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>