<div class="container">
            <nav class="navbar navbar-expand-xl navbar-light bg-white no-gutters nav-top-wrap">
                <a class="" href="#">
                    <div class="col-xl-3 img-wrap">
                        <img src="img/logo.png" alt="Logo" class="logo">
                    </div>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="col-xl-6 ml-4 mr-4">
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav nav-top">
                            <li class="nav-item">
                                <a class="nav-link" href="#">Directory</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Advice &amp Guidance</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Services</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">How We Work</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Advertise</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-3 btn-wrap">
                    <div class="collapse navbar-collapse header-buttons-wrap">
                        <div class="header-buttons">
                            <button type="button" class="btn navbar-btn  text-white">Sign Up</button>
                            <button type="button" class="btn navbar-btn  text-white">Log In</button>
                        </div>
                    </div>
                </div>
            </nav>
        </div>