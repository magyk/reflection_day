<div class="purple-banner">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class= "purple-banner-content">
                    <h1>Franchise Opportunities with Franchise Supermarket</h1>
                    <p>Starting your own business can be a daunting process, so Franchise Supermarket aims to ease this process and help you become one of the 97% of franchiesees within the UK that are achieving profitability.</p>
                    <button type="button" class="btn btn-success">Start Your Search <span class="entypo-right-open-big"></span></button>
                </div>
            </div>
        </div>
    </div>
</div>