<div class="nav-main-bg">
    <div class="container nav-main-wrap">
        <nav class="navbar navbar-inverse bg-inverse navbar-expand-lg nav-main">
            <button class="navbar-toggler navbar-dark" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="#" role="button">Industry &blacktriangledown;</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" role="button">Investment &blacktriangledown;</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" role="button">Locations &blacktriangledown;</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" role="button">Contact Us &blacktriangledown;</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" role="button">Search For A Franchise <i class="fa fa-search" aria-hidden="true"></i> </a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>