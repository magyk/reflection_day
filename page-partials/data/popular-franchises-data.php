<?php

    $popFranchises = [
        0 => [
            'src' => null,
            'owl-text' => null,
        ],
        1 => [
            'src' => 'img/popular_franchises/1.png',
            'owl-text' => 'EnviroVent provide energy saving products and services which improve the quality of the air in people\'s...',
        ],
        2 => [
            'src' => 'img/popular_franchises/2.png',
            'owl-text' => 'Set up a successful and profitable estate and letting agency with Go Direct Lettings and secure yourself with...',
        ],
        3 => [
            'src' => 'img/popular_franchises/3.png',
            'owl-text' => 'Join the market leader in offering customers the choice of either baked or unbaked pizza - or both with...',
        ],
        4 => [
            'src' => 'img/popular_franchises/4.png',
            'owl-text' => 'Start your own home care business with one of the first and best home care franchises within the...',
        ],
        5 => [
            'src' => 'img/popular_franchises/5.png',
            'owl-text' => 'Chemex as a full member of the bfa and is a UK based and owned franchise with a truly unique offering...',
        ],
        6 => [
            'src' => 'img/popular_franchises/6.png',
            'owl-text' => 'A Very Different Property Franchise Opportunity With No Experience Needed And High Profit Potential...',
        ],
    ];

?>

