<div class="orange-banner">
    <div class="container orange-banner-container">
        <div class="row justify-content-center">
            <!-- Orange Banner Cards -->
            <div class="col-sm">
                <div class="card-deck orange-banner-body position-absolute">
                    <div class="card">
                        <div class="card-body">
                            <h2 class="card-title text-center">Franchise Directory</h2>
                            <p class="card-text text-center">There's never been a better time for you to visit our franchise directory to find the best franchise for you. Franchise Supermarket is...</p>
                            <button type="button" class="btn m-2">View Directory ></button>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <h2 class="card-title text-center">Advice &amp Guidance</h2>
                            <p class="card-text text-center">Franchise Supermarket’s goal is to provide a friendly and relaxing setting for potential franchisees to browse and compare all types...</p>
                            <button type="button" class="btn m-2">Advice &amp Guidance ></button>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <h2 class="card-title text-center">Services For Franchisors</h2>
                            <p class="card-text text-center">Franchise Services are organisations that provide a unique service to either Franchisors, franchisees, prospective franchisors or...</p>
                            <button type="button" class="btn m-2">View Directory ></button>
                        </div>
                    </div>
                </div>
            </div>
         </div>
    </div>
</div>