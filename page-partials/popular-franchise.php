<?php include 'data/popular-franchises-data.php' ?>

<div class="container franchises mb-4">
    <h1 class="mt-4 mb-3">Popular Franchises</h1>
</div>
<div class="container owl-wrap">
    <div class="row">
        <div class="col">
            <div class="franchise-cards">
                <div class="owl-carousel">
                    <?php
                    foreach ($popFranchises as $index => $values) {
                        if ($values['src'] && $values['owl-text']) {
                    ?>
                        <div class="owl-slide">
                            <div class="owl-img">
                                <img class="" src="<?= $values['src'] ?>" alt="Card image cap" />
                                </div>
                            <div class="owl-body">
                                <p class="owl-text"><?= $values['owl-text'] ?></p>
                            </div>
                            <div class="owl-btn-wrap">
                                <button type="button" class="btn m-2">Find Out More ></button>
                            </div>
                        </div>
                    <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>