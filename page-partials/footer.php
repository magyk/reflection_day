        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <h4>Franchise Supermarket</h4>
                        <ul>
                            <li>Home</li>
                            <li>About Us</li>
                            <li>Contact Us</li>
                            <li>Franchise Directory</li>
                            <li>Advertise</li>
                            <li>Advice and Guidance</li>
                            <li>Local Franchise Opportunities</li>
                        </ul>
                    </div>
                    <div class="col-lg-3">
                        <h4 class="footer-2nd-col">Need to Know</h4>
                        <ul class="footer-2nd-col">
                            <li>Terms &amp; Conditions</li>
                            <li>Privacy Policy</li>
                            <li>Cookie Policy</li>
                            <li>Sitemap</li>
                        </ul>
                    </div>
                    <div class="col-lg-3">
                        <h4 class="footer-3rd-col">Social</h4>
                        <div class="row social-icons">
                            <div class="social-icon">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                            </div>
                            <div class="social-icon">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                            </div>
                            <div class="social-icon">
                                <i class="fa fa-instagram" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <h4>Contact</h4>
                        <ul>
                            <li class="footer-pad-bot">info&commat;franchisesupermarket.net</li>
                            <li>Franchise Supermarket Ltd</li>
                            <li>2nd Floor</li>
                            <li>Exchange House</li>
                            <li>Exchange Square</li>
                            <li>Beccles</li>
                            <li>Suffolk</li>
                            <li>NR34 9HH</li>
                            <li class="footer-pad-top">01502 717 500</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <ul class="footer-list">
                            <li class="footer-pad-bot">Copyright &copy; <?php echo date("Y");?> Franchise Supermarket. All Rights Reserved</li>
                            <li>Web Design by Netmatters</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
        <script src="js/owlCarousel2/owl.carousel.min.js"></script>
        <script src="js/js_plugin_initializer.js"></script>
</body>
</html>