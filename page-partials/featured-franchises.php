<div class="container featured-franchises">
    <div class="row">
        <h1>Featured Franchises</h1>
        <div class="card-group">
            <div class="row m-0">
                <div class="card">
                    <img class="card-img" src="img/featured_franchises/1.png" alt="Card image cap">
                </div>
                <div class="card">
                    <img class="card-img" src="img/featured_franchises/2.png" alt="Card image cap">
                </div>
                <div class="card">
                    <img class="card-img" src="img/featured_franchises/3.png" alt="Card image cap">
                </div>
                <div class="card">
                    <img class="card-img" src="img/featured_franchises/4.png" alt="Card image cap">
                </div>
                <div class="card">
                    <img class="card-img" src="img/featured_franchises/5.png" alt="Card image cap">
                </div>
                <div class="card">
                    <img class="card-img" src="img/featured_franchises/6.png" alt="Card image cap">
                </div>
            </div>
            <div class="row m-0">
                <div class="card">
                    <img class="card-img" src="img/featured_franchises/7.png" alt="Card image cap">
                </div>
                <div class="card">
                    <img class="card-img" src="img/featured_franchises/8.png" alt="Card image cap">
                </div>
                <div class="card">
                    <img class="card-img" src="img/featured_franchises/9.png" alt="Card image cap">
                </div>
                <div class="card">
                    <img class="card-img" src="img/featured_franchises/10.png" alt="Card image cap">
                </div>
                <div class="card">
                    <img class="card-img" src="img/featured_franchises/11.png" alt="Card image cap">
                </div>
                <div class="card">
                    <img class="card-img" src="img/featured_franchises/12.png" alt="Card image cap">
                </div>
            </div>
        </div>
    </div>
</div>